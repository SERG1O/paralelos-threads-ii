#include <stdio.h> 
#include <stdlib.h>  
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h> 
#include <Windows.h>
#include <time.h>

struct list_node_s {
    int data;
    struct list_node_s* next;
    pthread_mutex_t mutex;
};

const int IN_LIST = 1;
const int EMPTY_LIST = -1;
const int END_OF_LIST = 0;
int n;
int m;
float m_member_frac;
float m_insert_frac;
float m_delete_frac;
pthread_mutex_t head_mutex;
pthread_mutex_t count_mutex;

int count, count_m, count_i, count_d = 0;
int m_member, m_insert, m_delete;
int thread_count;
struct list_node_s* head_p = NULL;
int* opr_values;

// struct list_node_s* head_p
int Member(int value) {
    struct list_node_s* curr_p;
    pthread_mutex_lock(&head_mutex);
    //count_m++;
    curr_p = head_p;
    while (curr_p != NULL && curr_p->data < value) {
        if (curr_p->next != NULL) 
            pthread_mutex_lock(&(curr_p->next->mutex));
        if (curr_p == head_p) 
            pthread_mutex_unlock(&head_mutex);
        pthread_mutex_unlock(&(curr_p->mutex));
        curr_p = curr_p->next;
    }
    if (curr_p == NULL || curr_p->data > value) {
        if (curr_p == head_p)
            pthread_mutex_unlock(&head_mutex);
        if (curr_p != NULL)
            pthread_mutex_unlock(&(curr_p->mutex));
        return 0;
    }
    else {
        if (curr_p == head_p)
            pthread_mutex_unlock(&head_mutex);
        pthread_mutex_unlock(&(curr_p->mutex));
        return 1;
    }
    
}


void Init_ptrs(struct list_node_s** curr_pp, struct list_node_s** pred_pp) {
    *pred_pp = NULL;
    pthread_mutex_lock(&head_mutex);
    *curr_pp = head_p;
    if (head_p != NULL)
        pthread_mutex_lock(&(head_p->mutex));
}
int Advance_ptrs(struct list_node_s** curr_pp, struct list_node_s** pred_pp) {
    int rv = IN_LIST;
    struct list_node_s* curr_p = *curr_pp;
    struct list_node_s* pred_p = *pred_pp;

    if (curr_p == NULL)
        if (pred_p == NULL)
            return EMPTY_LIST;
        else
            return END_OF_LIST;
    else {
        if (curr_p->next != NULL)
            pthread_mutex_lock(&(curr_p->next->mutex));
        else
            rv = END_OF_LIST;
        if (pred_p != NULL)
            pthread_mutex_unlock(&(pred_p->mutex));
        else
            pthread_mutex_unlock(&head_mutex);
        *pred_pp = curr_p;
        *curr_pp = curr_p->next;
        return rv;
    }
}

int Delete(int value) {
    struct list_node_s* curr;
    struct list_node_s* pred;
    int rv = 1;

    Init_ptrs(&curr, &pred);

    /* Find value */
    while (curr != NULL && curr->data < value) {
        Advance_ptrs(&curr, &pred);
    }

    if (curr != NULL && curr->data == value) {
        if (pred == NULL) { /* first element in list */
            head_p = curr->next;

            pthread_mutex_unlock(&head_mutex);
            pthread_mutex_unlock(&(curr->mutex));
            pthread_mutex_destroy(&(curr->mutex));
            free(curr);
        }
        else {
            pred->next = curr->next;
            pthread_mutex_unlock(&(pred->mutex));

            pthread_mutex_unlock(&(curr->mutex));
            pthread_mutex_destroy(&(curr->mutex));
            free(curr);
        }
    }
    else { /* Not in list */
        if (pred != NULL)
            pthread_mutex_unlock(&(pred->mutex));
        if (curr != NULL)
            pthread_mutex_unlock(&(curr->mutex));
        if (curr == head_p)
            pthread_mutex_unlock(&head_mutex);
        rv = 0;
    }

    return rv;
}

int Insert(int value) {
    struct list_node_s* curr;
    struct list_node_s* pred;
    struct list_node_s* temp;
    int rv = 1;

    Init_ptrs(&curr, &pred);

    while (curr != NULL && curr->data < value) {
        Advance_ptrs(&curr, &pred);
    }

    if (curr == NULL || curr->data > value) {
        temp = (list_node_s*)malloc(sizeof(struct list_node_s));
        pthread_mutex_init(&(temp->mutex), NULL);
        temp->data = value;
        temp->next = curr;
        if (curr != NULL)
            pthread_mutex_unlock(&(curr->mutex));
        if (pred == NULL) {
            // Inserting in head of list
            head_p = temp;
            pthread_mutex_unlock(&head_mutex);
        }
        else {
            pred->next = temp;
            pthread_mutex_unlock(&(pred->mutex));
        }
    }
    else { /* value in list */
        if (curr != NULL)
            pthread_mutex_unlock(&(curr->mutex));
        if (pred != NULL)
            pthread_mutex_unlock(&(pred->mutex));
        else
            pthread_mutex_unlock(&head_mutex);
        rv = 0;
    }

    return rv;
}

void* thread_function(void* rank) {
    long my_rank = (long)rank;
    int i, val;
    unsigned seed = my_rank + 1;
    int my_member = 0, my_insert = 0, my_delete = 0;
    int ops_per_thread = m / thread_count;
//    printf(" ops_per_thread = %d\n", ops_per_thread);
    int inicio = ops_per_thread * my_rank;
//    printf(" inicio = %d\n", inicio);
    int real_member = ops_per_thread * m_member_frac;
    int real_insert = ops_per_thread * m_insert_frac;
    for (i = inicio; i < (inicio + ops_per_thread); i++) {
        if (my_member < real_member ) {
            Member(opr_values[i]);
            my_member++;
        }
        else if (my_insert < real_insert ) {
            Insert(opr_values[i]);
            my_insert++;
        }
        else {
            Delete(opr_values[i]);
            my_delete++;
        }
    }  

    pthread_mutex_lock(&count_mutex);
    count_m += my_member;
    count_i += my_insert;
    count_d += my_delete;
    pthread_mutex_unlock(&count_mutex);

    return NULL;
}
int main(int argc, char* argv[]) {    //argc : no. of arguments and argv : vector of arguments
    long thread;
    pthread_t* thread_handles;
    clock_t start, end;
    double cpu_time_used;
    srand(time(NULL));
    int i, ins_value;
    pthread_mutex_init(&head_mutex, NULL);
    pthread_mutex_init(&count_mutex, NULL);

    thread_count = 32;
    n = 10000;
    m = 100000;
    m_member_frac = 0.999;
    m_insert_frac = 0.0005;
    m_delete_frac = 0.0005;

    opr_values = (int*)malloc(m * sizeof(int));
    m_member = m * m_member_frac;
    m_insert = m * m_insert_frac;
    m_delete = m - (m_member + m_insert);
    //printf("%d : %d , %d:\n ", m_member, m_insert, m_delete);
    for (i = 0; i < n; i++)
    {
        ins_value = rand() % 65535; //value should be between 2^16 - 1
        Insert(ins_value);
    }
    for (i = 0; i < m; i++)
    {
        opr_values[i] = rand() % 65535; //value should be between 2^16 - 1
    }
    thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
    start = clock();
    //pthread_mutex_init(&mutex, NULL);
    for (thread = 0; thread < thread_count; thread++)
    {
        pthread_create(&thread_handles[thread], NULL, thread_function, (void*)thread);
    }
    for (thread = 0; thread < thread_count; thread++)
    {
        pthread_join(thread_handles[thread], NULL);
    }
    end = clock();
    cpu_time_used = (double(end) - double(start) ) / CLOCKS_PER_SEC;
    printf("El tiempo de ejecucion es : %f , con th %d:\n ", cpu_time_used, thread_count);
    printf("Total ops = %d\n", count_m+count_i+count_d);
    printf("member ops = %d\n", count_m);
    printf("insert ops = %d\n", count_i);
    printf("delete ops = %d\n", count_d);
    pthread_mutex_destroy(&head_mutex);
    pthread_mutex_destroy(&count_mutex);
    free(thread_handles);
    return 0;
}
