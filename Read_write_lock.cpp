#include <stdio.h> 
#include <stdlib.h>  
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h> 
#include <Windows.h>
#include <time.h>

struct list_node_s {
    int data;
    struct list_node_s* next;
};


int n;// numero de valores
int m;//numero de operaciones en cada ejecucion
float m_member_frac;
float m_insert_frac;
float m_delete_frac;

pthread_rwlock_t rwlock;


int pthread_rwlock_rdlock(pthread_rwlock_t* rwlock_p);
int pthread_rwlock_rdlock(pthread_rwlock_t* rwlock_p);
int pthread_rwlock_rdlock(pthread_rwlock_t* rwlock_p);

int count, count_m, count_i, count_d = 0;
int m_member, m_insert, m_delete;
int thread_count;
struct list_node_s* head_p = NULL;
int* opr_values;

// struct list_node_s* head_p
int Member(int value) {
    count_m++;
    struct list_node_s* curr_p = head_p;
    while (curr_p != NULL && curr_p->data < value)
        curr_p = curr_p->next;
    if (curr_p == NULL || curr_p->data > value) {
        return 0;
    }
    else {
        return 1;
    }
}
int Insert(int value) {
    //count_i++;
    struct list_node_s** head_pp = &head_p;
    struct list_node_s* curr_p = *head_pp;
    struct list_node_s* pred_p = NULL;
    struct list_node_s* temp_p;
    while (curr_p != NULL && curr_p->data < value) {
        pred_p = curr_p;
        curr_p = curr_p->next;
    }
    if (curr_p == NULL || curr_p->data > value) {
        temp_p = (list_node_s*)malloc(sizeof(struct list_node_s));
        temp_p->data = value;
        temp_p->next = curr_p;
        if (pred_p == NULL) //new first node
            *head_pp = temp_p;
        else
            pred_p->next = temp_p;
        return 1;
    }
    else {
        return 0; //value already in the list
    }
}

int Delete(int value) {
   // count_d++;
    struct list_node_s** head_pp = &head_p;
    struct list_node_s* curr_p = *head_pp;
    struct list_node_s* pred_p = NULL;
    while (curr_p != NULL && curr_p->data < value) {
        pred_p = curr_p;
        curr_p = curr_p->next;
    }
    if (curr_p != NULL && curr_p->data == value) {
        if (pred_p == NULL) {
            *head_pp = curr_p->next;  //deleting the first node in the list
            free(curr_p);
        }
        else {
            pred_p->next = curr_p->next;
            free(curr_p);
        }
        return 1;
    }
    else {
        return 0; //value not in the list
    }
}


void* thread_function(void* rank) {
    long my_rank = (long)rank;
    int i, val;
    int my_member = 0, my_insert = 0, my_delete = 0;
    int ops_per_thread = m / thread_count;
    //    printf(" ops_per_thread = %d\n", ops_per_thread);
    int inicio = ops_per_thread * my_rank;
    //    printf(" inicio = %d\n", inicio);
    int real_member = ops_per_thread * m_member_frac;
    int real_insert = ops_per_thread * m_insert_frac;
  //  printf("%d %d %d %d\n", my_rank,inicio, (inicio + ops_per_thread),real_member);
    for (i = inicio; i < (inicio + ops_per_thread); i++) {
        if (my_member < real_member) {
            pthread_rwlock_rdlock(&rwlock);
            Member(opr_values[i]);
            my_member++;
            pthread_rwlock_unlock(&rwlock);
            
        }
        else if (my_insert < real_insert) {
  //          printf(" %d\n", my_insert);
            pthread_rwlock_wrlock(&rwlock);
            Insert(opr_values[i]);
            my_insert++;
            pthread_rwlock_unlock(&rwlock);
            
        }
        else {
            pthread_rwlock_wrlock(&rwlock);
            Delete(opr_values[i]);
            pthread_rwlock_unlock(&rwlock);
            my_delete++;
        }
    }

    pthread_rwlock_wrlock(&rwlock);
    //count_m += my_member;
    count_i += my_insert;
    count_d += my_delete;
    pthread_rwlock_unlock(&rwlock);

    return NULL;
}
int main(int argc, char* argv[]) {    //argc : no. of arguments and argv : vector of arguments
    long thread;
    pthread_t* thread_handles;
    clock_t start, end;
    double cpu_time_used;
    srand(time(NULL));
    int i, ins_value;
    pthread_rwlock_init(&rwlock, NULL);

    thread_count = 2;
    n = 10000;
    m = 100000;
    m_member_frac = 0.999;
    m_insert_frac = 0.0005;
    m_delete_frac = 0.0005;

    opr_values = (int*)malloc(m * sizeof(int));
    m_member = m * m_member_frac;
    m_insert = m * m_insert_frac;
    m_delete = m - (m_member + m_insert);
    for (i = 0; i < n; i++)
    {
        ins_value = rand() % 65535; //value should be between 2^16 - 1
        Insert(ins_value);
    }
    for (i = 0; i < m; i++)
    {
        opr_values[i] = rand() % 65535; //value should be between 2^16 - 1
    }
    thread_handles = (pthread_t*)malloc(thread_count * sizeof(pthread_t));
    start = clock();
    //pthread_mutex_init(&mutex, NULL);
    for (thread = 0; thread < thread_count; thread++)
    {
        pthread_create(&thread_handles[thread], NULL, thread_function, (void*)thread);
    }
    for (thread = 0; thread < thread_count; thread++)
    {
        pthread_join(thread_handles[thread], NULL);
    }
    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
    //FILE* pFile;
    //pFile = fopen("data-mutex.txt", "a");
    //fprintf(pFile, "%f\n", cpu_time_used);
    printf("El tiempo de ejecucion es : %f , con th %d:\n ", cpu_time_used, thread_count);
    printf("Total ops = %d\n", count_m + count_i + count_d);
    printf("member ops = %d\n", count_m);
    printf("insert ops = %d\n", count_i);
    printf("delete ops = %d\n", count_d);
    free(thread_handles);
    pthread_rwlock_destroy(&rwlock);
    return 0;

}